import os


REMOVE_PATHS = [
    '{% if cookiecutter.is_cli == "no" %} {{cookiecutter.project_slug}}/__main__.py {% endif %}',
]

for path in REMOVE_PATHS:
    path = path.strip()
    print("Delete ", os.getcwd(), path)
    if path and os.path.exists(path):
        if os.path.isdir(path):
            os.rmdir(path)
        else:
            os.unlink(path)